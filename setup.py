from setuptools import setup

setup(
    name="DLKit edX Runtime",
    version="0.1.0",
    description="Digital Learning ToolKit edX-based Runtime",
    author="Jeff Merriman",
    author_email="birdland@mit.edu",
    url="https://mc3.mit.edu",
    license="MIT",
    install_requires=[
    ]
)